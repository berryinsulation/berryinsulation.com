/**
 * Module dependencies
 */
var express = require('express'),
    controllers = require('../controllers');

/**
 * the new Router exposed in express 4
 * the indexRouter handles all requests to the `/` path
 */
var router = express.Router();

router.get('/', function(req, res) {
 res.render('index');
});

router.get('/projects/', function(req, res) {
 res.render('projects/index');
})

router.get('/project/', function(req, res) {
 res.render('project/index');
})

router.get('/process/', function(req, res) {
 res.render('process/index');
})

router.get('/about/', function(req, res) {
 res.render('about/index');
})

router.get('/contact/', function(req, res) {
 res.render('contact/index');
})

router.get('/resouce/', function(req, res) {
 res.render('resource/index');
})

router.get('/resources/', function(req, res) {
 res.render('resources/index');
})

router.get('/testimonials/', function(req, res) {
 res.render('testimonials/index');
})

router.get('/blog/', function(req, res) {
 res.render('blog/index');
})

router.get('/services/', function(req, res) {
 res.render('services/index');
})
exports.router = router;
