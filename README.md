Berry Insulation
================

The code in the source files do two main tasks. One they allow a developer to run berryinsulation.com via a node.js server. And two they build a static site, via Gulp ( hosted in the berry.cms folder ), that allow the developer to integrate the source files with Siteleaf.net ( client facing CMS ).
