<?php
//If the form is submitted
if(isset($_POST['submit'])) {
         
    //Check to make sure sure that a valid email address or main phone is submitted
    if((trim($_POST['email']) == '') && (trim($_POST['home_phone']) == '')) {
        $hasError = true;
        $emailphoneError = true;
    } else if ((trim($_POST['email']) != '') && (!eregi("^[A-Z0-9._%-]+@[A-Z0-9._%-]+\.[A-Z]{2,4}$", trim($_POST['email'])))) {
        $hasError = true;
        $emailError = true;
        $email_from = trim($_POST['email']);
	$home_phone = trim($_POST['home_phone']);
    } else {
        $email_from = trim($_POST['email']);
	$home_phone = trim($_POST['home_phone']);
    }
    
    //Check to make sure that the name field is not empty
    if(trim($_POST['name']) == '') {
	$hasError = true;
        $nameError = true;
    } else {
	$name = trim($_POST['name']);
    }


    //Check to make sure that the message field is not empty
    if(trim($_POST['comments']) == '') {
	$hasError = true;
        $messageError = true;
    } else {
	$comments = trim($_POST['comments']);
    }
    
    $address = trim($_POST['address']);
    $city = trim($_POST['city']);
    $state = trim($_POST['state']);
    $zip = trim($_POST['zip']);
    $cell_phone = trim($_POST['cell_phone']);
    $home_type = trim($_POST['home_type']);
    $siding = trim($_POST['siding']);
    $square_feet = trim($_POST['square_feet']);
    $garage = trim($_POST['garage']);
    $attic_access = trim($_POST['attic_access']);
    $basement = trim($_POST['basement']);

   
    function clean_string($string) {       
        $bad = array("content-type","bcc:","to:","cc:","href");       
        return str_replace($bad,"",$string);     
    }  
             
    //If there is no error, send the email
    if(!isset($hasError)) {
        $email_to = "mike.setta12@gmail.com";           
        $email_subject = "Website Contact Us Submission";                 
        
        $email_message .= "Name: ".clean_string($name)."\n";     
        $email_message .= "Main Phone: ".clean_string($home_phone)."\n";     
        $email_message .= "Email: ".clean_string($email_from)."\n";     
        $email_message .= "Message: ".clean_string($comments)."\n";             
        $email_message .= "Address: ".clean_string($address)."\n";     
        $email_message .= "City: ".clean_string($city)."\n";     
        $email_message .= "State: ".clean_string($state)."\n";     
        $email_message .= "Zip: ".clean_string($zip)."\n";     
        $email_message .= "Additional Phone: ".clean_string($cell_phone)."\n";     
        $email_message .= "Home Type: ".clean_string($home_type)."\n";             
        $email_message .= "Siding: ".clean_string($siding)."\n";             
        $email_message .= "Square Feet: ".clean_string($square_feet)."\n";             
        $email_message .= "Garage: ".clean_string($garage)."\n";             
        $email_message .= "Attic Access: ".clean_string($attic_access)."\n";             
        $email_message .= "Basement: ".clean_string($basement)."\n";             
        
        // create email headers 
        $headers = 'From: '.$email_from."\r\n". 'Reply-To: '.$email_from."\r\n" . 'X-Mailer: PHP/' . phpversion(); 
        @mail($email_to, $email_subject, $email_message, $headers);   
        $emailSent = true;
    }
}
?>

<html>


<head>
    <title>Berry Insulation</title>
    <link href="menu.css" rel="stylesheet" type="text/css" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js" type="text/javascript"></script>
</head>
<body>
    <div id="container">
        <div>
            <img src="Header-800-v2.png" />
        </div>
        <div id="biggieLightBlueFont">
            SAVE 20-50%<br />
            ON RISING HOME ENERGY COSTS
        </div>
        <div id="menu">
            <a href="/">Home</a> <a href="/services">
                Services</a> <a href="http://contact.berryinsulation.com">Contact</a>
        </div>
        <div style="margin:20px;margin-left:50px;">
            <div>
                <?php if(isset($hasError)) { //If errors are found 
                            if (isset($emailphoneError)) {
                ?>
                        <p class="error">Please enter either your email address or phone number.</p>
                <?php           } 
                        if (isset($emailError)) {
                           ?>
                        <p class="error">Please enter a valid email address.</p>
                <?php  }
                        
                        if (isset($nameError)) {
                            ?>
                        <p class="error">Please enter your name.</p>
                <?php }
                         if (isset($messageError)) {
                           ?>
                        <p class="error">Please enter a message as to how we can help you.</p>
                <?php  }
                       }?>

                <?php if(isset($emailSent) && $emailSent == true) { //If email is sent ?>
                        <p><strong>Email Successfully Sent!</strong></p>
                        <p>Thank you <strong><?php echo $name;?></strong> for using our contact form! Your email was successfully sent and we will be in touch with you soon.</p>
                <?php } ?>

                <font class="kindaBiggieBlackFont">For More Information</font><br />
                <p>The mission of <b>Berry Insulation Company</b> is to assist home owners in reducing energy consumption and utility costs.  <br/>We constantly are looking for more affordable ways to help our customers.  <br/>Please contact us by calling us direct at (216) 621-6290 or e-mail us by filling out the information below.</p>
                <form name="contactus" id="contactus" method="post" action="http://contact.berryinsulation.com"> 
                    <table> 
                        <tr>  
                            <td valign="top" width="140px">   
                                <label for="name">Name</label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="name" maxlength="100" size="75" class="required" value="<?php if(isset($name)) { echo $name; } ?>">  
                            </td> 
                        </tr>   
                        <tr>  
                            <td valign="top">   
                                <label for="home_phone">Phone</label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="home_phone" maxlength="30" size="30" class="required" value="<?php if(isset($home_phone)) { echo $home_phone; } ?>">  
                            </td>
                        </tr>
                        <tr>  
                            <td valign="top">   
                                <label for="email">Email Address</label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="email" maxlength="80" size="75" class="required email" value="<?php if(isset($email_from)) { echo $email_from; } ?>">  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="comments">Message</label>  
                            </td>  
                            <td valign="top">   
                                <textarea name="comments" maxlength="1000" cols="60" rows="6" class="required" text="<?php if(isset($comments)) { echo $comments; } ?>"></textarea>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top" colspan="2">   
                                <BR /><b>Optional Information to help us serve you better.  </b>
                            </td>  
                         </tr> 
                         <tr>  
                            <td valign="top">   
                                <label for="address">Address</label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="address" maxlength="250" size="75" value="<?php if(isset($address)) { echo $address; } ?>">  
                            </td>
                        </tr>
                        <tr>  
                            <td valign="top">   
                                <label for="city">City, State & Zip</label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="city" maxlength="30" size="30" value="<?php if(isset($city)) { echo $city; } ?>">  
                                 <input  type="text" name="state" maxlength="10" size="10" value="<?php if(isset($state)) { echo $state; } ?>">  
                                 <input  type="text" name="zip" maxlength="10" size="20" value="<?php if(isset($zip)) { echo $zip; } ?>">  
                            </td>
                        </tr>
                        <tr>  
                            <td valign="top">   
                                <label for="cell_phone">Additional Phone </label>  
                            </td>  
                            <td valign="top">   
                                <input  type="text" name="cell_phone" maxlength="30" size="30" value="<?php if(isset($cell_phone)) { echo $cell_phone; } ?>">  
                            </td>
                        </tr>
                       <tr>  
                            <td valign="top">   
                                <label for="home_type">Type of Home</label>  
                            </td>  
                            <td valign="top">   
                                <select name="home_type">   
                                    <option value="Colonial"<?php if(isset($home_type) && ($home_type == "Colonial")) { echo " Selected"; } ?>>Colonial</option>   
                                    <option value="Bungalo"<?php if(isset($home_type) && ($home_type == "Bungalo")) { echo " Selected"; } ?>>Bungalo</option>   
                                    <option value="Ranch"<?php if(isset($home_type) && ($home_type == "Ranch")) { echo " Selected"; } ?>>Ranch</option>  
                                    <option value="Split Level"<?php if(isset($home_type) && ($home_type == "Split Level")) { echo " Selected"; } ?>>Split Level</option>  
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="siding">Siding</label>  
                            </td>  
                            <td valign="top">   
                                <select name="siding">   
                                    <option value="Brick"<?php if(isset($siding) && ($siding == "Brick")) { echo " Selected"; } ?>>Brick</option>   
                                    <option value="Wood"<?php if(isset($siding) && ($siding == "Wood")) { echo " Selected"; } ?>>Wood</option>   
                                    <option value="Aluminum"<?php if(isset($siding) && ($siding == "Aluminum")) { echo " Selected"; } ?>>Aluminum</option>  
                                    <option value="Vinyl"<?php if(isset($siding) && ($siding == "Vinyl")) { echo " Selected"; } ?>>Vinyl</option>  
                                    <option value="Other"<?php if(isset($siding) && ($siding == "Other")) { echo " Selected"; } ?>>Other</option>  
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="square_feet">Square Feet</label>  
                            </td>  
                            <td valign="top">   
                                <select name="square_feet">   
                                    <option value="Under 1000"<?php if(isset($square_feet) && ($square_feet == "Under 1000")) { echo " Selected"; } ?>>Under 1,000</option>   
                                    <option value="1001 to 1500"<?php if(isset($square_feet) && ($square_feet == "1001 to 1500")) { echo " Selected"; } ?>>1,001 - 1,500</option>   
                                    <option value="1501 to 2000"<?php if(isset($square_feet) && ($square_feet == "1501 to 2000")) { echo " Selected"; } ?>>1,501 - 2,000</option>  
                                    <option value="Over 2000"<?php if(isset($square_feet) && ($square_feet == "Over 2000")) { echo " Selected"; } ?>>Over 2,000</option>  
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="garage">Garage</label>  
                            </td>  
                            <td valign="top">   
                                <select name="garage">   
                                    <option value="Attached"<?php if(isset($garage) && ($garage == "Attached")) { echo " Selected"; } ?>>Attached</option>   
                                    <option value="Detached"<?php if(isset($garage) && ($garage == "Detached")) { echo " Selected"; } ?>>Detached</option>   
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="attic_access">Attic Access</label>  
                            </td>  
                            <td valign="top">   
                                <select name="attic_access">   
                                    <option value="Hatch"<?php if(isset($attic_access) && ($attic_access == "Hatch")) { echo " Selected"; } ?>>Hatch</option>   
                                    <option value="Pull Down Ladder"<?php if(isset($attic_access) && ($attic_access == "Pull Down Ladder")) { echo " Selected"; } ?>>Pull Down Ladder</option>   
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td valign="top">   
                                <label for="basement">Basement</label>  
                            </td>  
                            <td valign="top">   
                                <select name="basement">   
                                    <option value="Full Finished Basement"<?php if(isset($basement) && ($basement == "Full Finished Basement")) { echo " Selected"; } ?>>Full Finished Basement</option>   
                                    <option value="Partially Finished Basement"<?php if(isset($basement) && ($basement == "Partially Finished Basement")) { echo " Selected"; } ?>>Partially Finished Basement</option>   
                                    <option value="Crawl Space"<?php if(isset($basement) && ($basement == "Crawl Space")) { echo " Selected"; } ?>>Crawl Space</option>  
                                    <option value="Slab"<?php if(isset($basement) && ($basement == "Slab")) { echo " Selected"; } ?>>Slab</option>  
                                </select>  
                            </td>   
                        </tr> 
                        <tr>  
                            <td colspan="2" style="text-align:center">   
                                &nbsp;  
                            </td> 
                        </tr> 
                        <tr>  
                            <td colspan="2" style="text-align:center">   
                                <input type="submit" value="Send Message" name="submit">  
                            </td> 
                        </tr> 
                        <tr>  
                            <td colspan="2" style="text-align:center">   
                                &nbsp;  
                            </td> 
                        </tr> 
                    </table> 
                </form> 
            </div>
        </div>
        <div id="footer">
            Copyright &copy; 2011 Berry Insulation. All Rights Reserved.
        </div>
    </div>
</body>
</html>
