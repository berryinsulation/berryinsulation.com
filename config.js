var config = {
  development: {
    server: {
      port: 29192,
    },
    database: {
      url: 'mongodb://localhost/taylor-studio_dev'
    }
  },
  testing: {
    server: {
      port: 3001
    },
    database: {
      url: 'mongodb://localhost/taylor-studio_test'
    }
  },
  production: {
    server: {
      port: 8080
    },
    database: {
      url: 'mongodb://localhost/taylor-studio'
    }
  }
};

module.exports = config[process.env.NODE_ENV || 'development'];
