var gulp = require('gulp'),
  path = require('path'),
  merge = require('merge-stream'),
  imagemin = require('gulp-imagemin'),
  compass = require('gulp-compass'),
  pngcrush = require('imagemin-pngcrush'),
  jpegtran = require('imagemin-jpegtran'),
  runSequence = require('run-sequence'),
  watch = require('gulp-watch'),
  plumber = require('gulp-plumber'),
  concat = require('gulp-concat'),
  jade = require('gulp-jade'),
  imageResize = require('gulp-image-resize'),
  copy = require('gulp-copy'),
  uglify = require('gulp-uglify'),
  minifyCSS = require('gulp-minify-css'),
  livereload = require('gulp-livereload'),
  rename = require('gulp-rename');

// Create Static site for cms
gulp.task('templates', function() {
  gulp.src(['!./views/projects/*.jade', '!./views/project/*.jade', './views/**/*.jade', './views/index.jade', '!./views/{templates,templates/**}', '!./views/{includes,includes/**}'], {base: './views'})
    .pipe(jade({
      data: {
        cms: true
      }
    }))
    .pipe(rename({
      basename: "default"
    }))
    .pipe(gulp.dest('./berry.cms/'))
});

gulp.task('templates__project', function() {
  gulp.src(['./views/project/*.jade'], {base: './views'})
    .pipe(jade({
      data: {
        cms: true
      }
    }))
    .pipe(rename({
      basename: "default",
      dirname: "projects"
    }))
    .pipe(gulp.dest('./berry.cms/'))
});

gulp.task('templates__projects', function() {
  gulp.src(['./views/projects/*.jade'], {base: './views'})
    .pipe(jade({
      data: {
        cms: true
      }
    }))
    .pipe(rename({
      basename: "index",
      dirname: "projects"
    }))
    .pipe(gulp.dest('./berry.cms/'))
});

gulp.task('compass', function() {
  return gulp.src('public/sass/**/style.scss')
    .pipe(compass({
      css: 'public/css', 
      sass: 'public/sass', 
      image: 'public/images'
    }))
    .pipe(gulp.dest('public/css'));
})

gulp.task('concat-css', function() {
  return gulp.src(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/vendor/glidejs/dist/css/glide.core.css', 'public/vendor/glidejs/dist/css/glide.theme.css', 'public/vendor/featherlight/src/featherlight.css', 'public/css/style.css'])
    .pipe(concat('concat.css'))
    .pipe(gulp.dest('public/css'));
});

gulp.task('minimize-css', function() {
  return gulp.src('public/css/concat.css')
    .pipe(minifyCSS())
    .pipe(rename('style.min.css'))
    .pipe(gulp.dest('public/css'));
});

gulp.task('compass-gt-ie8', function() {
  return gulp.src('public/sass/**/gt-ie8.scss')
    .pipe(compass({
      css: 'public/css', 
      sass: 'public/sass', 
      image: 'public/images'
    }))
    .pipe(gulp.dest('public/css'));
})

gulp.task('concat-css-gt-ie8', function() {
  return gulp.src(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/css/glide.core.css', 'public/css/glide.theme.css', 'public/vendor/featherlight/src/featherlight.css', 'public/css/gt-ie8.css'])
    .pipe(concat('concat-gt-ie8.css'))
    .pipe(gulp.dest('public/css'));
});

gulp.task('minimize-css-gt-ie8', function() {
  return gulp.src('public/css/concat-gt-ie8.css')
    .pipe(minifyCSS())
    .pipe(rename('gt-ie8.min.css'))
    .pipe(gulp.dest('public/css'));
});

gulp.task('concat-js', function() {
  return gulp.src(['public/vendor/jQuery.mmenu/src/js/jquery.mmenu.min.all.js', 'public/vendor/featherlight/src/featherlight.js', 'public/vendor/glidejs/dist/glide.js', 'public/vendor/jQuery.mmenu/src/js/jquery.mmenu.min.all.js', 'public/vendor/youtubeBackground/src/jquery.youtubebackground.js', 'public/vendor/masonry/dist/masonry.pkgd.min.js'])
    .pipe(concat('concat.js'))
    .pipe(gulp.dest('public/js'));
});

gulp.task('uglify-js', function() {
  return gulp.src('public/js/concat.js')
    .pipe(uglify())
    .pipe(rename('main.min.js'))
    .pipe(gulp.dest('public/js/'));
});
gulp.task('watch', function() {
  gulp.watch(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/vendor/glidejs/dist/css/glide.css', 'public/vendor/featherlight/src/featherlight.css', 'public/sass/style.scss'], ['optimize-css']);
  gulp.watch('public/sass/gt-ie8.scss', ['optimize-css-gt-ie8']);
  gulp.watch(['./public/js/**/*.js', './public/vendor/**/*.js', '!./public/js/concat.js', '!./public/js/main.min.js'], ['optimize-js']);
  gulp.watch(['./views/**/*.jade'], ['templates', 'templates__project', 'templates__projects']);
  gulp
    .src(['./views/**/*.jade', './public/css/**/*.min.css', './public/js/**/concat.js'])
    .pipe(watch())
    .pipe(livereload(35727));
});


//gulp.task('minimize-css', function() {
  //return gulp.src('public/css/concat.css')
    //.pipe(minifyCSS())
    //.pipe(rename('style.min.css'))
    //.pipe(gulp.dest('public/css'));
//});

gulp.task('optimize-css', 
  function() {
    runSequence('compass', 'concat-css', 'minimize-css');
});

gulp.task('optimize-css-gt-ie8', 
  function() {
    runSequence('compass-gt-ie8', 'concat-css-gt-ie8', 'minimize-css-gt-ie8');
});

gulp.task('optimize-js', 
  function() {
    runSequence('concat-js', 'uglify-js');
});

gulp.task('default', 
  function() {
    runSequence('watch', 'optimize-css', 'optimize-css-gt-ie8', 'optimize-js', 'templates', 'templates__project', 'templates__projects'); });
//gulp.task('optimize-css-gt-ie8', function() {
  //gulp.src('public/sass/**/gt-ie8.scss')
    //.pipe(compass({
      //css: 'public/css', 
      //sass: 'public/sass', 
      //image: 'public/images'
    //}))
    //.pipe(gulp.dest('public/css'))
    //.pipe(gulp.src(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/css/glide.css', 'public/vendor/featherlight/src/featherlight.css', 'public/css/gt-ie8.css']))
    //.pipe(concat('concat-gt-ie8.css'))
    //.pipe(gulp.dest('public/css'))
    //.pipe(gulp.src('public/css/concat-gt-ie8.css'))
    //.pipe(minifyCSS())
    //.pipe(rename('gt-ie8.min.css'))
    //.pipe(gulp.dest('public/css'));
//});

gulp.task('min-img-slideshow', function () {
  gulp.src(['./public/images/slideshow/*'])
    .pipe(imageResize({ 
      width : 1200,
      upscale : false
    }))
    .pipe(imagemin({
      progressive: true,
      use: [jpegtran(), pngcrush()]
    }))
    .pipe(gulp.dest('./public/images/min/slideshow'));
});

gulp.task('min-img-process', function () {
  gulp.src(['./public/images/process/*'])
    .pipe(imageResize({ 
      width : 380,
      upscale : false
    }))
    .pipe(imagemin({
      progressive: true,
      use: [pngcrush()]
    }))
    .pipe(gulp.dest('./public/images/min/process'));
});


gulp.task('min-img-projects', function () {
  gulp.src(['./public/images/slideshow/*'])
    .pipe(imageResize({ 
      width : 700,
      upscale : false
    }))
    .pipe(imagemin({
      progressive: true,
      use: [pngcrush()]
    }))
    .pipe(gulp.dest('./public/images/min/projects'));
});

//gulp.task('default', ['optimize-css', 'watch', 'templates']);
//gulp.task('default', ['optimize-css', 'optimize-css-gt-ie8', 'watch', 'templates']);

//gulp.task('copy', function(){
  //gulp.src('./build/blog.html')
    //.pipe(gulp.dest('./build/blog/'));
//});


// Compile Our Sass
//gulp.task('compass', function() {
  //gulp.src('public/sass/**/style.scss')
    //.pipe(compass({
      //css: 'public/css', 
      //sass: 'public/sass', 
      //image: 'public/images'
    //}))
    //.pipe(gulp.dest('public/css')) Don't need this b/c the css file is set above.  That's my theory at least
    //.pipe(gulp.dest('public/css'));
//});

//gulp.task('concat', function() {
  //gulp.src(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/css/glide.css', 'public/vendor/featherlight/src/featherlight.css', 'public/css/style.css', ''])
    //.pipe(concat('concat.css'))
    //.pipe(gulp.dest('public/css'));
//});

//gulp.task('minify-css', function() {
  //gulp.src('public/css/concat.css')
    //.pipe(minifyCSS())
    //.pipe(rename('style.min.css'))
    //.pipe(gulp.dest('public/css'));
//});

//gulp.task('compass-gt-ie8', function() {
  //gulp.src('public/sass/**/gt-ie8.scss')
    //.pipe(compass({
      //css: 'public/css', 
      //sass: 'public/sass', 
      //image: 'public/images'
    //}))
    //.pipe(gulp.dest('public/css')) Don't need this b/c the css file is set above.  That's my theory at least
    //.pipe(gulp.dest('public/css'));
//});

//gulp.task('concat-gt-ie8', function() {
  //gulp.src(['public/css/normalize.css', 'public/vendor/jQuery.mmenu/src/css/jquery.mmenu.all.css', 'public/css/animate.css', 'public/css/glide.css', 'public/vendor/featherlight/src/featherlight.css', 'public/css/style.css', ''])
    //.pipe(concat('concat-gt-ie8.css'))
    //.pipe(gulp.dest('public/css'));
//});

//gulp.task('minify-css-gt-ie8', function() {
  //gulp.src('public/css/concat-gt-ie8.css')
    //.pipe(minifyCSS())
    //.pipe(rename('style.min.css'))
    //.pipe(gulp.dest('public/css'));
//});

